# searches for connected devices and returns a tuple containg devices and its properties
import evdev

def searchDevices():
    # get a list of connected devices using evdev
    connected_devices = [evdev.InputDevice(path) for path in evdev.list_devices()]
    
    if len(connected_devices) > 0 :     # if we have connected devices, i.e no. of devices (length of the connected_devices list) > 0
        sorted_devices = [] # create an empty list, this will be later used to hold devices and their properties in tuples, eg [(device1,devicename),(device2,device2name)]
        for device in connected_devices:                   # loop through every device
            devices_info = [(device.name, device.path)]    # for every device create its own list containing a tuple
            sorted_devices = sorted_devices + devices_info # add the list to the sorted_devices list
        print("Connected Devices found:", sorted_devices)
        return tuple(sorted_devices)                       # when all devices have been looped over convert the sorted_devices list into a tuple and return it. 
            
    else :                  # if we do not have any connected device, no. of devices = 0
        print("No new devices were found")
