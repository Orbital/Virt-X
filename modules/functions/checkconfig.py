# checks if config file for virt-x is present, if not then create one

# requires python's builtin os library
import os

def checkConfig(config_file_path):
    # check if the file exists in the given location
    if os.path.exists(config_file_path):
        print("Configuration was found.\nUsing the configuration file at :", config_file_path)
    else:
        # if the file is not present then create one
        os.system('mkdir -p ~/.config/virt-x/ && touch ~/.config/virt-x/virt-x.ini')
        print("Configuration could not be found.\nA new configuration file was created at :", config_file_path  )
