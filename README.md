<p align="center" >
<img src="./.codeberg/logo.png" width="60" />
</p>

<h1 align="center">Virt-X</h1>

<div align="center">

![platform](https://img.shields.io/static/v1?label=Platform&message=Linux&style=for-the-badge&logo=linux&color=9cf)
![status](https://img.shields.io/static/v1?label=Status&message=Work-in-progress&style=for-the-badge&logo=Codeberg&color=9cf)
![release](https://img.shields.io/static/v1?label=Release&message=Alpha&style=for-the-badge&logo=Git&color=9cf)

</div>


<div align="center">

Emulate any generic game-pad (controller) as a Xbox Controller on Linux.

</div>


## Dependencies

<div align="center">

## System
| Package Name      | Description                        | Installation                     | Requirement |
|-------------------|:-----------------------------------|:---------------------------------|:-----------|
| [Xboxdrv]       | Userspace drivers for Xbox & XBox 360 controllers  | System package-manager dependent | **Essential**  |
| [Evtest] | Input device event monitor and query tool   | System package-manager dependent | **Essential**   |
| [python2-dbus]         | Python2 bindings for dbus       | System package-manager dependent | **Essential**, ONLY REQUIRED by `Fedora`/`Redhat` based distributions along with `Xboxdrv and Evtest`  |

## Python
| Package Name      | Description                        | Installation                     | Requirement |
|-------------------|:-----------------------------------|:---------------------------------|:-----------|
| [PyGObject]       | GTK 3 Development libraries for python   | `pip install PyGObject` | **Essential**  |
| [Evdev] | Provides bindings to the Linux input handling subsystem    | `pip install evdev` | **Essential**   |


</div>


### Developer Resources

* [PyGTK Documentation](https://docs.gtk.org/gtk3/index.html)
* [PyGTK Examples](https://python-gtk-3-tutorial.readthedocs.io/en/latest/index.html)
* [Evdev Documentation](python-evdev.readthedocs.io/en/latest/)


[Xboxdrv]: https://pkgs.org/search/?q=xboxdrv
[Evtest]: https://pkgs.org/search/?q=evtest
[PyGObject]: https://pypi.org/project/PyGObject/
[Evdev]: https://pypi.org/project/evdev/