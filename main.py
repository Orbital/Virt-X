import os.path
# import the Gtk module, specifically version GTK+3 to access all GTK +3 classes and functions
import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk

#import custom modules
from modules.functions.checkconfig import checkConfig 
from modules.functions.searchdevices import searchDevices 

# before the GTK loop starts, silently check if a config file exists. if it does not then create one 

# the config file should be at /home/user/.config/virt-x/virt-x.ini
# a directory cannot be created without knowing the user's name (as in /user/) hence relative path is used i.e ~/.config/virt-x/virt-x.ini
# python cannot do file operations with relative paths
# the relative path is taken and then converted into an absolute path using python's os library
config_file_path = os.path.abspath(os.path.expanduser(os.path.expandvars('~/.config/virt-x/virt-x.ini')))
checkConfig(config_file_path)

# create a window class, which inherits properties from Gtk.Window class
class MainWindow(Gtk.Window):
    def __init__(self):
        Gtk.Window.__init__(self)

        # set window properties
        self.set_title("Virt-X")             # title
        self.set_default_size(600,400)       # dimensions on startup, (height,width in pixels)
        self.set_border_width(0)             # set border width
        
        # create a container to hold everything
        main_container = Gtk.Box(orientation = Gtk.Orientation.VERTICAL, spacing=5)

        #create a container to hold menus this will be under main container
        menu_box = Gtk.Box(orientation = Gtk.Orientation.VERTICAL, spacing=5)
        # this menubar should be inside menu_box and hold all menus
        menu_bar = Gtk.MenuBar()

        #adapter menu
        adapter_menu = Gtk.Menu()                               # menu for searching connected devices
        adapter_menu_dropdown = Gtk.MenuItem(label="Adapter")   # dropdown to show menu items when adapter_menu is clicked
        adapter_menu_dropdown.set_submenu(adapter_menu)         # the drop down is a submenu for adapter_menu
        adapter_search = Gtk.MenuItem(label="Search")           # adapter_menu items - search function
        adapter_exit = Gtk.MenuItem(label="Exit")               # adapter_menu items - quit function
        # connect menu with a function on click
        adapter_search.connect("activate", self.scanDevices)    
        adapter_exit.connect("activate", Gtk.main_quit)
        # add adapter_menu items to the menu
        adapter_menu.append(adapter_search)                     
        adapter_menu.append(adapter_exit)                       
       
        #devices menu
        device_menu = Gtk.Menu()                               
        device_menu_dropdown = Gtk.MenuItem(label="Devices")  
        device_menu_dropdown.set_submenu(device_menu)         
        device_show_configured = Gtk.CheckMenuItem(label="Show configured devices only")
        device_menu.append(device_show_configured) 

        #preferences menu
        pref_menu = Gtk.Menu()                               
        pref_menu_dropdown = Gtk.MenuItem(label="Preferences")  
        pref_menu_dropdown.set_submenu(pref_menu)         
        pref_autostart = Gtk.CheckMenuItem(label="Autostart")
        pref_disable_xpad = Gtk.CheckMenuItem(label="Disable Xpad")
        pref_menu.append(pref_autostart)  
        pref_menu.append(pref_disable_xpad) 

        #help menu
        help_menu = Gtk.Menu()                               
        help_menu_dropdown = Gtk.MenuItem(label="Help")  
        help_menu_dropdown.set_submenu(help_menu)         
        help_xbox_layout = Gtk.MenuItem(label="Official Xbox 360 Gamepad layout")
        help_report = Gtk.MenuItem(label="Report a problem")
        help_about = Gtk.MenuItem(label="About Virt-X")
        help_menu.append(help_xbox_layout)  
        help_menu.append(help_report)  
        help_menu.append(help_about)  
        
        # add the dropdown menus to the menubar
        menu_bar.append(adapter_menu_dropdown)                  
        menu_bar.append(device_menu_dropdown)                  
        menu_bar.append(pref_menu_dropdown)                  
        menu_bar.append(help_menu_dropdown)                  
        
        # create a box layout to contain all other layouts for the main area, this will be under main_container
        main_box = Gtk.Box(orientation = Gtk.Orientation.VERTICAL, spacing=10)
        
        label = Gtk.Label()
        label.set_label("main window")

        #add all components to their respective containers
        menu_box.pack_start(menu_bar, False, False, 0)          # hexpand = false, vexpand= false
        main_box.pack_start(label, False, False, 0)             # hexpand = false, vexpand= false
        
        # add the containers to the main_container
        main_container.pack_start(menu_box, False, False, 0)     # hexpand = false, vexpand= false
        main_container.pack_start(main_box, False, False, 0)     # hexpand = false, vexpand= false
        
        # add the main_container to the window
        self.add(main_container)
    
    # function on click of buttons/menus will call function 'searchDevices' to scan for connected devices
    def scanDevices(self, widget):
        searchDevices()

# initialize a window and set destroy/delete event
window = MainWindow()
window.connect("delete-event", Gtk.main_quit)


# display window
window.show_all()


# start the GTK+ proscessing loop
Gtk.main()
